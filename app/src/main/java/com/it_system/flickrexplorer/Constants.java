package com.it_system.flickrexplorer;

/**
 * Created by Tomasz Pająk on 2019-05-31.
 */
public class Constants {
    public static final String URL = "https://www.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1";
}
